Вопрос 1 В чем недостаток маршрутизации между VLAN методом Router-on-a-Stick? 
- Требует использования большего количества физических интерфейсов, чем традиционная маршрутизация между VLAN 
- Не поддерживает пакеты с тегами VLAN 
+ Не масштабируется более чем на 50 VLAN 
- Требует использования нескольких интерфейсов маршрутизатора, сконфигурированных для работы в качестве каналов доступа 

Вопрос 2 Где должен быть назначен ІР-адрес при настройке маршрутизатора как части топологии маршрутизации между сетями VLAN типа Router-on-a-Stick? 
+ На подынтерфейсе 
- На интерфейсе 
- Ha VLAN 
- Ha SVI 

Вопрос 3 Какая команда используется для удаления только VLAN 20 с коммутатора? 
+ no vlan 20 
- delete flashvlan dat 
- по switchport access vlan 20 
- delete vlan.dat 

Вопрос 4 Какая команда отображает тип инкапсуляции, идентификатор голосовой VLAN и режим доступа VLAN для интерфейса Fа0/1? 
+ show interfaces fa0/1 switchport 
- show vlan brief 
- show mac address-table interface fa0/1 
- show interfaces trunk 

Вопрос 5 Небольшой колледж использует VLAN 10 для сети класса и VLAN 20 для офисной сети. Что необходимо для обеспечения связи между этими двумя сетями VLAN при использовании устаревшей маршрутизации между сетями VLAN? 
- Для подключения к SVI на коммутаторе необходим маршрутизатор с одним интерфейсом VLAN 
+ Необходимо использовать маршрутизатор как минимум с двумя интерфейсами LAN 
- Необходимы две группы коммутаторов, каждая с портами, настроенными для одной VLAN 
- Для подключения к маршрутизатору необходим коммутатор с портом, настроенным как транковый 

Вопрос 6 Порт Fа0/11 на коммутаторе назначается для VLAN 30. Что произойдет, если ввести команду по switchport access vlan 30 на интерфейсе Fа0/11? 
- Порт Fа0/11 будет отключен 
- VLAN 30 будет удалена 
+ Порт Fа0/11 будет возвращен в VLAN 1 
- Будет отображено сообщение об ошибке 

Вопрос 7 Посмотрите на рисунок. Солько подынтерфейсов необходимо создать администратору, чтобы каждая VLAN могла маршрутизироваться? 
- 1
- 2
+ 4
- 3
img 7.png

Вопрос 8 Посмотрите на рисунок. В каком режиме коммутации должен быть назначен порт G0/1, если используются лучшие практики Cisco? 
- access 
- native 
- auto 
+ trunk 
img 8.png

Вопрос 9 Посмотрите на рисунок. ПК-А и ПК-В находятся в VLAN 60. ПК-А не может связаться с ПК-В. В чем может быть проблема? 
- Собственная (Native) VLAN должна быть VLAN 60 
- Транк был настроен с помощью команды switchport nonegotiate 
+ VLAN, используемая компьютером РС-А, отсутствует в списке разрешенных VLAN на магистрали (Trunk) 
- Собственная (Native) VLAN отсекается от канала 
img 9.png

Вопрос 10 Посмотрите на рисунок. Сетевой администратор настраивает маршрутизацию между VLAN в сети. На данный момент используется только одна VLAN, но скоро будет добавлено больше. Какой недостающий параметр выделен вопросительным знаком? 
- Количество хостов, разрешенных на интерфейсе 
- Идентификатор подынтерфейса 
+ Hoмep VLAN 
- Идентификатор типа используемой инкапсуляции 
img 10.png

Вопрос 11 Посмотрите на рисунок. Сетевой администратор проверяет конфигурацию маршрутизации между VLAN. Пользователи жалуются, что ПК2 не может связаться с ПК1. Основываясь на выводе предположите, какова возможная причина проблемы? 
- Команда по shutdown не введена на подынтерфейсах 
- На интерфейсе Gi0/0 не настроен ІР-адрес 
+ Команда encapsulation dot1Q 5 содержит неправильную VLAN 
- Gi0/0 не настроен в качестве транкового порта 
img 11.png

Вопрос 12 Посмотрите на рисунок. Сетевой администратор просматривает назначения портов и VLAN на коммутаторе S2 и замечает, что интерфейсы Gi 0/1 и Gi 0/2 не включены в выходные данные. Предположите, почему в выводе отсутствуют интерфейсы? 
- Они административно выключены 
- Между коммутаторами имеется несовпадение собственных VLAN 
- К интерфейсам не подключены кабели 
+ Они настроены как транковые интерфейсы 
img 12.png

Вопрос 13 Посмотрите на рисунок. Согласно выходным данным команды show running-config, для VLAN 15, 30 и 45 была реализована конфигурация Router-on-a-Stick. ПК в VLAN 45, которые используют сеть 172.16.45.0/24, не могут подключиться к ПК в VLAN 30 в сети 172.16.30.0/24. Какая ошибка, скорее всего, вызывает эту проблему? 
- в GigabitEthernet 0/0.30 отсутствует команда по shutdown 
- Интерфейс GigabitEthernet 0/0 не имеет IP-адреса 
- На GigabitEthernet 0/0.45 настроена неправильная сеть VLAN 
+ На GigabitEthernet 0/0.30 настроен неверный ІР-адрес 
img 13.png

Вопрос 14 Посмотрите на рисунок. Технический специалист настраивает коммутатор SW3 для управления голосовым трафиком и трафиком данных через порт Fa0/20. Что не так с конфигурацией? 
- Команда mis qos trust cos должна относиться к сети VLAN 35 
- В настройке нет ничего плохого 
+ Команда, используемая для назначения голосовой VLAN порту коммутатора, неверна 
- Интерфейсу fа0/20 может быть назначена только одна VLAN 
img 14.png

Вопрос 15 Университет использует сеть VLAN15 для лабораторной сети и сеть VLAN30 для сети преподавателей. Что требуется для обеспечения связи между этими двумя виртуальными локальными сетями при использовании подхода Route-on-a-Stick? 
- Требуется маршрутизатор как минимум с двумя интерфейсами LAN 
- Необходимы две группы коммутаторов, каждая с портами, настроенными для одной VLAN 
- Нужен многоуровневый коммутатор 
+ Коммутатор с портом, настроенным как транк, нужен при подключении к маршрутизатору 

Вопрос 16 Что должен сделать сетевой администратор, чтобы удалить порт FastEthernet fa0/1 из VLAN 2 и назначить его VLAN 3? 
+ Введите команду switchport access vlan 3 в режиме конфигурации интерфейса 
- Введите команды по vlan 2 и vlan 3 в режиме глобальной конфигурации 
- Введите команду по shutdown в режиме конфигурации интерфейса, чтобы вернуть его к конфигурации по умолчанию, а затем настройте порт для VLAN 3 
- Введите команду switchport trunk native vlan 3 в режиме конфигурации интерфейса 

Вопрос 17 Что происходит с портом, связанным с VLAN 10, когда администратор удаляет VLAN 10 с коммутатора? 
+ Порт становится неактивным 
- Порт возвращается к VLAN по умолчанию 
- Порт автоматически ассоциируется с собственной VLAN 
- Порт снова создает VLAN 

Вопрос 18 Что характерно для традиционной маршрутизации между VLAN? 
- В топологии может быть использована только одна VLAN 
- VLAN пользователя должен иметь тот же идентификационный номер, что и VLAN управления 
+ Маршрутизатору требуется отдельный канал Ethernet для каждой VLAN 
- Маршрутизация между VLAN должна выполняться на коммутаторе, а не на маршрутизаторе 

Вопрос 19 Заполните пробел. Используйте полный синтаксис команды. Команда отображает назначение VLAN для всех портов, а также существующие VLAN на коммутаторе. 
+ show vlan 

Вопрос 20 В настоящее время коммутатор Cisco разрешает трафик, помеченный VLAN 10 и 20, через магистральный порт Fа0/5. Каков эффект от команды switchport trunk allowed vlan 30 на Fа0/5? 
- Это позволяет реализовать собственную (Native) VLAN из 30 на Fа0/5 
- Это позволяет использовать VLAN с 1 по 30 на Fа0/5 
- Это позволяет использовать VLAN 10, 20 и 30 на Fa0/5 
+ Это позволяет использовать только VLAN 30 на Fа0/5 