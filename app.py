from flask import Flask, render_template, request, redirect, url_for, session
import os
import random
import datetime

app = Flask(__name__)

# Конфигурация сессий
app.config['SECRET_KEY'] = 'your_secret_key'
app.config['SESSION_TYPE'] = 'filesystem'
app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(days=1)  # Сессия будет храниться 1 день

DATA_DIR = 'static/data'


def load_questions():
    tests = {}
    for test_dir in os.listdir(DATA_DIR):
        test_path = os.path.join(DATA_DIR, test_dir)
        if os.path.isdir(test_path):
            test_file = os.path.join(test_path, f'{test_dir.split()[0]}_data.txt')
            with open(test_file, 'r', encoding='utf-8') as f:
                lines = f.readlines()
                questions = {}
                question = {}
                question_id = None
                last = ''
                for line in lines:
                    line = line.strip()
                    if line.startswith('Вопрос'):
                        last = 'Вопрос'
                        if question_id and question:
                            questions[question_id] = question
                        question_id = f"{test_dir}_{line.split(' ')[1]}"
                        question = {'question': [line], 'test_dir': test_dir, 'answers': [], 'correct': [], 'images': [], 'matching': {}}
                    elif line.startswith('- '):
                        last = '-'
                        question['answers'].append(line[2:].strip())
                    elif line.startswith('. '):
                        question['answers'][-1] += '\n' + line[2:].strip()
                        if last == '+':
                            question['correct'][-1] += '\n' + line[2:].strip()
                    elif line.startswith('+ '):
                        last = '+'
                        question['answers'].append(line[2:].strip())
                        question['correct'].append(line[2:].strip())
                    elif line.startswith('img '):
                        last = 'img'
                        question['images'].append(line[4:].strip())
                    elif line.startswith('~ '):
                        last = '~'
                        key, value = line[2:].split(' ~ ')
                        question['matching'][key.strip()] = value.strip()
                    else:
                        if last == 'Вопрос':
                            question['question'] += [line]

                if question_id and question:
                    questions[question_id] = question
                tests[test_dir] = questions
    return tests


def load_progress():
    return session.get('progress', {})


def save_progress(progress):
    session['progress'] = progress
    session.permanent = True  # Делает сессию постоянной


def reset_progress(testname):
    progress = load_progress()
    if testname in progress:
        del progress[testname]
    save_progress(progress)


questions_data = load_questions()


@app.route('/')
def index():
    return render_template('index.html', tests=sorted(questions_data.keys(), key=lambda x: int(x.split()[0][1:])))


@app.route('/test/<test_name>')
def test(test_name):
    if test_name not in questions_data:
        return "Test not found", 404
    question_ids = list(questions_data[test_name].keys())
    random.shuffle(question_ids)
    session[f'{test_name}_questions'] = question_ids
    session[f'{test_name}_progress'] = {}
    return redirect(url_for('question', test_name=test_name, question_num=1))


@app.route('/exam')
def exam():
    exam_questions = []
    for test_name in questions_data:
        exam_questions.extend(list(questions_data[test_name].items()))
    random.shuffle(exam_questions)
    exam_questions = exam_questions[:50]
    session['exam_questions'] = [q[0] for q in exam_questions]
    session['exam_progress'] = {}
    return redirect(url_for('question', test_name='exam', question_num=1))


@app.route('/allq')
def allq():
    all_questions = []
    for test_name in questions_data:
        all_questions.extend(list(questions_data[test_name].items()))
    random.shuffle(all_questions)
    session['all_questions'] = [q[0] for q in all_questions]
    session['all_progress'] = {}
    return redirect(url_for('question', test_name='all', question_num=1))


@app.route('/<test_name>/question/<int:question_num>', methods=['GET', 'POST'])
def question(test_name, question_num):
    if test_name == 'all':
        question_ids = session.get('all_questions', [])
    elif test_name == 'exam':
        question_ids = session.get('exam_questions', [])
    else:
        question_ids = session.get(f'{test_name}_questions', [])

    if not question_ids or question_num < 1 or question_num > len(question_ids):
        return "Question not found", 404

    question_id = question_ids[question_num - 1]
    question = questions_data[question_id.split('_')[0]][question_id]

    if test_name == 'all':
        progress_data = session.get('all_progress', {})
    elif test_name == 'exam':
        progress_data = session.get('exam_progress', {})
    else:
        progress_data = session.get(f'{test_name}_progress', {})

    if request.method == 'POST':
        correct_answers = question['correct']
        if question['matching']:
            answers = {}
            for key in question['matching'].keys():
                answers[key] = request.form.get(key)
            is_correct = all(question['matching'][key] == answers[key] for key in answers)
        else:
            answers = request.form.getlist('answers')
            if not answers:
                answers = [request.form.get('answer')]
            is_correct = all(answer in correct_answers for answer in answers) and len(answers) == len(correct_answers)

        progress_data[question_id] = (answers, is_correct)
        if test_name == 'all':
            session['all_progress'] = progress_data
        elif test_name == 'exam':
            session['exam_progress'] = progress_data
        else:
            session[f'{test_name}_progress'] = progress_data

    next_question_num = question_num + 1 if question_num < len(question_ids) else 0
    prev_question_num = question_num - 1 if question_num > 1 else 0
    result = progress_data.get(question_id)

    shuffled_options = list(question['matching'].values())
    random.shuffle(shuffled_options)

    return render_template('question.html', test_name=test_name, question=question, question_num=question_num,
                           next_question_num=next_question_num, prev_question_num=prev_question_num, result=result,
                           shuffled_options=shuffled_options)


@app.route('/result/<test_name>')
def result(test_name):
    force = request.args.get('force', 'false').lower() == 'true'
    if test_name == 'all':
        progress_data = session.get('all_progress', {})
        question_ids = session.get('all_questions', [])
    elif test_name == 'exam':
        progress_data = session.get('exam_progress', {})
        question_ids = session.get('exam_questions', [])
    else:
        progress_data = session.get(f'{test_name}_progress', {})
        question_ids = session.get(f'{test_name}_questions', [])

    if not question_ids:
        return render_template('unanswered.html', test_name=test_name, unanswered_questions=[(q_id, question_ids.index(q_id) + 1) for q_id in question_ids])

    unanswered_questions = [(q_id, question_ids.index(q_id) + 1) for q_id in question_ids if q_id not in progress_data or not progress_data[q_id][0]]
    if unanswered_questions and not force:
        return render_template('unanswered.html', test_name=test_name, unanswered_questions=unanswered_questions)
    print(progress_data)
    results = [(question_ids.index(q_id) + 1, q_id, progress_data[q_id]) if q_id in progress_data else (question_ids.index(q_id) + 1, q_id, False) for q_id in question_ids]
    return render_template('result.html', test_name=test_name, results=results, questions=questions_data, unanswered_questions=[num for _, num in unanswered_questions])


@app.route('/reset/<test_name>')
def reset(test_name):
    reset_progress(test_name)
    return redirect(url_for('index'))


@app.route('/search', methods=['GET', 'POST'])
def search():
    query = request.form.get('query', '')
    results = []
    if query:
        for test_name, questions in questions_data.items():
            for question_id, question in questions.items():
                if query.lower() in ' '.join(question['question']).lower():
                    results.append((test_name, question_id, question))
    return render_template('search.html', query=query, results=results)


if __name__ == '__main__':
    from flask_session import Session
    Session(app)
    app.run(debug=True)
